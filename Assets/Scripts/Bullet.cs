using UnityEngine;

public class Bullet : MonoBehaviour
{
    Rigidbody2D rb;

    private float aliveTime;
    private float radius;

    private GameObject explosionPrefab;

    private void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
        explosionPrefab = Resources.Load<GameObject>("Prefabs/Explosion");

        aliveTime = 3f;
        radius = 2f;

        // Si el projectil no col�lisiona abans del seu temps de vida, explotar�
        Invoke("Explode", aliveTime);
        Invoke("EnableCollider", .15f);
    }

    private void Update()
    {
        Vector2 v = rb.velocity;
        // Actualitza la seva rotaci� per a que caigui fluidament
        float angle = Mathf.Atan2(v.y, v.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    public void Initialize(float power, bool facingRight)
    {
        /**
         * Aquest metode s'encarrega de inicialitzar el projectil.
         * Primer, decideix a quina direcci� apuntar (depenent de si dispara el jugador o l'enemic).
         * Despr�s, s'impulsa amb la for�a rebuda i fent servir Impulse de ForceMode2D.
         */
        Vector3 dir;
        if (facingRight)
            dir = transform.right;
        else
            dir = -transform.right;

        rb.AddForce(dir * (power/4), ForceMode2D.Impulse);
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        /**
         * Un cop colisiona explota, comproba si la col�lisi� ha sigut amb el jugador o l'enemic (dins del radi)
         * i crida el m�tode de GameManager per afegir dany.
         */
        Explode();
        Collider2D playerCollide = Physics2D.OverlapCircle(transform.position, radius, LayerMask.GetMask("Player"));
        Collider2D enemyCollide = Physics2D.OverlapCircle(transform.position, radius, LayerMask.GetMask("Enemy"));

        if (playerCollide != null)
        {
            GameManager.DoDamage(true);
        }
        if (enemyCollide != null)
        {
            GameManager.DoDamage(false);
        }
    }

    void EnableCollider()
    {
        GetComponent<BoxCollider2D>().enabled = true;
    }

    void Explode()
    {
        /**
         * Aquest m�tode s'encarrega de crear l'explosi� (fent servir el sistema de part�cules)
         * Despr�s, destrueix el projectil (a si mateix) i l'explosio despr�s d'un segon.
         */
        GameObject expl = Instantiate(explosionPrefab, transform.position, transform.rotation);
        expl.transform.position = new Vector3(expl.transform.position.x, expl.transform.position.y, -10);

        Destroy(gameObject);
        Destroy(expl, 1f);
    }
}
