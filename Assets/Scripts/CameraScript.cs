using UnityEngine;

public class CameraScript : MonoBehaviour
{
    private Camera currentCamera;

    private float followSpeed;
    public Transform Target;
    private Vector3 offset;
    private  float y;

    private float size;

    PlayerController playerScript;
    EnemyScript enemyScript;

    private void Start()
    {
        currentCamera = GetComponent<Camera>();

        playerScript = GameObject.Find("PlayerTank").GetComponent<PlayerController>();
        enemyScript = GameObject.Find("EnemyTank").GetComponent<EnemyScript>();

        followSpeed = 4f;
        y = transform.position.y;
        offset = Target.position - transform.position;

        size = currentCamera.orthographicSize;
    }

    private void Update()
    {
        /**
         * Segueix al jugador o enemic depenent de qui t� el turn.
         * Quan un dels dos comen�a a apuntar, s'allunya i es mou cap a la dreta o esquerra (depenent de qui t� el torn).
         * Tots els moviments de la camara s�n fluits gr�cies als metodes Slerp de Vector3 i MoveTowards de Mathf.
         */
        Vector3 newPosition = Target.position - offset;
        newPosition.y = y;

        if (!playerScript.state.hasTurn)
            newPosition.x -= 13f;

        if (playerScript.state.shooting || enemyScript.state.aiming)
        {
            if (playerScript.state.hasTurn)
                newPosition.x += 5f;
            else
                newPosition.x -= 5f;
            size = 8.6f;
        }
        else
        {
            size = 5.6f;
        }

        currentCamera.orthographicSize = Mathf.MoveTowards(currentCamera.orthographicSize, size, followSpeed * Time.deltaTime);

        transform.position = Vector3.Slerp(transform.position, newPosition, followSpeed * Time.deltaTime);
        
    }

}
