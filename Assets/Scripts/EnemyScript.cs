using System.Collections;
using UnityEngine;

public class EnemyScript : MonoBehaviour
{
    // Classe per desar i consultar l'estat actual de l'enemic
    public class EnemyState
    {
        public bool moving = false;
        public bool aiming = false;
        public bool shooting = false;
        public bool hasTurn = false;
        public int health = GameManager.MaxHealth;
    }

    public EnemyState state = new EnemyState();

    private float velocitat;

    private Rigidbody2D rb;
    private float shootingAngle;
    private float currentPower;
    protected float currentAngle;

    private Transform turret;
    private GameObject bulletPrefab;
    private PlayerController player;

    public Animator enemyAnimator;

    public AudioSource[] sound;
    /* 0 -> Engine Idle
     * 1 -> Engine Moving
     * 2 -> Turret Aim
     * 3 -> Destroyed
     * 4 -> Hit
     */

    bool moveLeft = false;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        player = GameObject.Find("PlayerTank").GetComponent<PlayerController>();
        velocitat = 2f;

        state.hasTurn = false;
        shootingAngle = 35.0f;
        currentPower = 0f;
        currentAngle = 0f;

        turret = transform.Find("Pivot");
        bulletPrefab = Resources.Load<GameObject>("Prefabs/Bullet");

        sound = GetComponents<AudioSource>();
        enemyAnimator = GetComponent<Animator>();

        rb.position = new Vector2(rb.position.x, rb.position.y + 5f);
    }

    private void OnEnable()
    {
        state.hasTurn = true;
    }

    private void OnDisable()
    {
        state.hasTurn = false;
        state.aiming = false;
        state.shooting = false;
        state.moving = false;
    }

    void LateUpdate()
    {
        if (state.hasTurn)
        {
            if (!sound[0].isPlaying)
                sound[0].Play();

            // Si l'enemic NO esta apuntat, es mour�
            if (!state.aiming)
            {
                // Si encara no s'est� movent, decidir� en quina direcci� moure's
                if (!state.moving)
                {
                    // Hi ha un 50% de probabilitat de que es mogui cap a l'esquerra o la dreta
                    if (Random.Range(0, 101) < 50)
                        moveLeft = true;
                    else
                        moveLeft = false;

                    state.moving = true;
                }
                if (state.moving)
                {
                    float x = transform.localScale.x;

                    if (moveLeft)
                        rb.velocity = new Vector2(x * velocitat, rb.velocity.y);
                    else
                        rb.velocity = new Vector2(-x * velocitat, rb.velocity.y);

                    if (!sound[1].isPlaying)
                        sound[1].Play();

                    StartCoroutine(Move());
                }
            }

            turret.rotation = Quaternion.Euler(0, 0, transform.eulerAngles.z - currentAngle);

            // Si l'enemic NO s'esta movent, comen�ar� a apuntar
            if (state.aiming)
            {
                if (!sound[2].isPlaying)
                    sound[2].Play();

                if (Mathf.Round(currentAngle) != shootingAngle)
                {
                    // Si l'angle es mes alt que l'angle predefinit, apuntar� mes baix. Si �s mes baix, apuntar� mes alt.
                    if (Mathf.Round(currentAngle) > shootingAngle)
                        CalculateAngle(false);
                    else
                        CalculateAngle(true);
                }
                else
                {
                    state.aiming = false;
                    if (sound[2].isPlaying)
                        sound[2].Stop();

                    state.shooting = true;
                    FireProjectile();
                    StartCoroutine(ChangeTurn());
                }
            }
        }
        else
        {
            state.aiming = true;
            shootingAngle = Random.Range(25, 45);
        }

        if (!state.moving && sound[1].isPlaying)
            sound[1].Stop();
    }

    private void FixedUpdate()
    {
        rb.AddForce(-transform.up * 90f);
    }

    private IEnumerator Move()
    {
        yield return new WaitForSeconds(2f);
        state.moving = false;
        state.aiming = true;
    }

    private void CalculateAngle(bool up)
    {
        if (up)
            currentAngle += 0.1f;
        else
            currentAngle -= 0.1f;

        currentAngle = Mathf.Clamp(currentAngle, 0, 75);
    }

    private void FireProjectile()
    {
        /**
         * Aquest m�tode s'encarrega de disparar el projectil.
         * Primer, calcula quanta for�a utilitzar (segons la posicio del jugador i l'angle predefinit).
         * Despr�s, crea el projectil en la posici� de la torreta i l'impulsa amb la for�a calculada anteriorment.
         */
        currentPower = CalculateVelocity(player.transform, shootingAngle);
        GameObject bullet = Instantiate(bulletPrefab, turret.transform.position, turret.transform.rotation);
        bullet.GetComponent<Bullet>().Initialize(currentPower, false);

    }

    private float CalculateVelocity(Transform target, float angle)
    {
        /**
         * Metode extret i modificat del projecte base de la PEC 3.
         */
        var dir = target.position - transform.position;
        var h = dir.y;
        dir.y = 0;
        var dist = dir.magnitude;
        var a = angle * Mathf.Deg2Rad;

        dist += h / Mathf.Tan(a);

        // Modificat
        // L'enemic te un 50% de probabilitat d'apuntar correctament al jugador
        float aim = 0f;
        if (Random.Range(0, 101) < 50)
            aim = 1.35f;
        else
            aim = Random.Range(1.2f, 1.8f);

        return Mathf.Sqrt(dist * Physics.gravity.magnitude / Mathf.Sin(2 * a)) * aim * 3;
    }

    private IEnumerator ChangeTurn()
    {
        /**
         * Aquest metode s'encarrega de canviar de torn de manera fluida perqu� no sigui massa r�pid o abrupte.
         */
        state.hasTurn = false;
        yield return new WaitForSeconds(1.5f);

        state.shooting = false;
        currentAngle = 0;
        currentPower = 0;
        player.state.hasTurn = true;
    }
}
