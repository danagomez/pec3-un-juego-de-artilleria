using System.Collections;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    // Constants per definir el temps i la vida maximes.
    public const int MaxHealth = 5;
    public const int TotalTime = 120;

    private static PlayerController playerScript;
    private static EnemyScript enemyScript;
    private static CameraScript cameraScript;

    private static GameObject startMenu;
    private static GameObject endMenu;
    private static Text timeText;
    private static Text statsText;
    private static Text endText;
    private static Slider playerSlider;
    private static Slider enemySlider;

    private static int timeRemaining;
    public static bool paused;

    void Start()
    {
        cameraScript = Camera.main.GetComponent<CameraScript>();
        playerScript = GameObject.Find("PlayerTank").GetComponent<PlayerController>();
        playerScript.enabled = false;
        enemyScript = GameObject.Find("EnemyTank").GetComponent<EnemyScript>();

        cameraScript.Target = playerScript.transform;
        timeRemaining = TotalTime;

        startMenu = GameObject.Find("StartMenu");
        endMenu = GameObject.Find("EndMenu");
        timeText = GameObject.Find("TimeText").GetComponent<Text>();
        statsText = GameObject.Find("StatsText").GetComponent<Text>();
        endText = GameObject.Find("EndText").GetComponent<Text>();

        endMenu.SetActive(false);

        playerSlider = GameObject.Find("PlayerHealth").GetComponent<Slider>();
        playerSlider.maxValue = playerScript.state.health;
        playerSlider.value = playerScript.state.health;

        enemySlider = GameObject.Find("EnemyHealth").GetComponent<Slider>();
        enemySlider.maxValue = enemyScript.state.health;
        enemySlider.value = enemyScript.state.health;

        Time.timeScale = 0f;

        // Si el jugador ha reinicat el joc, el menu d'inici no es mostrar� i s'entrar� directament al joc.
        if (GameStats.Restarted)
            StartGame();
        else
            paused = true;

        GameStats.PlayerHits = 0;
        GameStats.EnemyHits = 0;
    }

    void Update()
    {
        if (!paused)
        {
            if (playerScript.state.hasTurn)
            {
                cameraScript.Target = playerScript.transform;
                playerScript.enabled = true;
                enemyScript.enabled = false;
            }
            else
            {
                cameraScript.Target = enemyScript.transform;
                playerScript.enabled = false;
                enemyScript.enabled = true;
            }
        }
    }

    private void LateUpdate()
    {
        // Si el jugador o l'enemic no tenen vida, s'acaba el joc
        if (playerScript.state.health <= 0 || enemyScript.state.health <= 0)
        {
            StartCoroutine(Death());
        }
    }

    public void StartGame()
    {
        /**
         * Aquest m�tode inicialitza el joc principal, activant el temps i reduint el temps restant.
         */
        startMenu.SetActive(false);
        Time.timeScale = 1f;
        InvokeRepeating(nameof(DecreaseTime), 0, 1);
        paused = false;

        playerScript.sound[0].Play();
    }

    public void RestartGame()
    {
        GameStats.Restarted = true;
        GameStats.TotalGames++;
        SceneManager.LoadScene("Game");
    }

    public void QuitGame()
    {
        Debug.Log("Sortint del joc...");
        Application.Quit();
    }

    private void DecreaseTime()
    {
        timeRemaining--;
        // Si s'acaba el temps, s'acaba el joc
        if (timeRemaining < 0)
        {
            endText.text = "S'ha acabat el temps!";
            EndGame();
        }
        else
            timeText.text = "Temps: " + timeRemaining;
    }

    IEnumerator Death()
    {
        /**
         * Si un dels jugadors mor, s'espera a que acabi l'animaci� de morir i crida EndGame().
         */
        playerScript.enabled = false;
        enemyScript.enabled = false;
        yield return new WaitForSeconds(playerScript.playerAnimator.GetCurrentAnimatorStateInfo(0).length + 2);

        if (playerScript.state.health <= 0)
            endText.text = "Has perdut!";
        else
            endText.text = "Has guanyat!";

        EndGame();
    }

    void EndGame()
    {
        /**
         * Aquest m�tode para els sons, activa el men� final i mostra les estadistiques finals del joc.
         */
        foreach (AudioSource audio in playerScript.sound)
        {
            if (audio.isPlaying)
                audio.Stop();
        }

        foreach (AudioSource audio in enemyScript.sound)
        {
            if (audio.isPlaying)
                audio.Stop();
        }

        endMenu.SetActive(true);
        var eventSystem = EventSystem.current;
        eventSystem.SetSelectedGameObject(GameObject.Find("RestartButton"), new BaseEventData(eventSystem));
        Time.timeScale = 0f;

        GameStats.TotalTime = (TotalTime - timeRemaining) + " segons";
        statsText.text = "Temps - " + GameStats.TotalTime + 
            "\nCops jugador - " + GameStats.PlayerHits + 
            "\nCops enemic - " + GameStats.EnemyHits + 
            "\nPartides totals - " + GameStats.TotalGames;
    }

    public static void DoDamage(bool player)
    {
        /**
         * Aquest m�tode s'encarrega de restar punts de vida al jugador o l'enemic.
         * Tamb� actualitza les barres de vida del UI, i utilitza l'animaci� i so adients segons la situaci�
         * (distingeix entre nom�s rebre dany o morir)
         */
        if (player)
        {
            playerScript.state.health--;
            playerSlider.value = playerScript.state.health;
            GameStats.PlayerHits++;

            if (playerScript.state.health != 0)
            {
                playerScript.playerAnimator.SetTrigger("Damage");
                playerScript.sound[4].Play();
            }
            else
            {
                playerScript.playerAnimator.SetTrigger("Death");
                playerScript.sound[3].Play();
            }
        }
        else
        {
            enemyScript.state.health--;
            enemySlider.value = enemyScript.state.health;
            GameStats.EnemyHits++;

            if (enemyScript.state.health != 0)
            {
                enemyScript.enemyAnimator.SetTrigger("Damage");
                enemyScript.sound[4].Play();
            }
            else
            {
                enemyScript.enemyAnimator.SetTrigger("Death");
                enemyScript.sound[3].Play();
            }
        }
    }
}
