
public static class GameStats
{
    /**
     * Aquesta classe s'encarrega de desar les estadistiques finals del joc.
     */

    public static bool Restarted = false;
    public static string TotalTime = "";
    public static int PlayerHits = 0;
    public static int EnemyHits = 0;
    public static int TotalGames = 1;
}
