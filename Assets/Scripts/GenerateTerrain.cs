using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class GenerateTerrain : MonoBehaviour
{

    public static int[,] mapArray;

    private Tilemap tilemap;
    public TileBase tile;
    private int terrainWidth;
    private int terrainHeight;

    void Start()
    {
        tilemap = GetComponent<Tilemap>();
        terrainWidth = 300;
        terrainHeight = 30;

        Generate();
    }

    void Generate()
    {
        float seed = Random.Range(.1f, 1f);

        mapArray = GenerateArray(terrainWidth, terrainHeight, true);
        RenderMap(PerlinNoiseSmooth(mapArray, seed, 10), tilemap, tile);
    }

    public static int[,] GenerateArray(int width, int height, bool empty)
    {
        int[,] map = new int[width, height];
        for (int x = 0; x < map.GetUpperBound(0); x++)
        {
            for (int y = 0; y < map.GetUpperBound(1); y++)
            {
                if (empty)
                {
                    map[x, y] = 0;
                }
                else
                {
                    map[x, y] = 1;
                }
            }
        }
        return map;
    }

    public static void RenderMap(int[,] map, Tilemap tilemap, TileBase tile)
    {
        //Clear the map (ensures we dont overlap)
        tilemap.ClearAllTiles();
        //Loop through the width of the map
        for (int x = 0; x < map.GetUpperBound(0); x++)
        {
            //Loop through the height of the map
            for (int y = 0; y < map.GetUpperBound(1); y++)
            {
                // 1 = tile, 0 = no tile
                if (map[x, y] == 1)
                {
                    tilemap.SetTile(new Vector3Int(x, y, 0), tile);
                }
            }
        }
    }

    public static int[,] PerlinNoiseSmooth(int[,] map, float seed, int interval)
    {
        int newPoint, points;
        //Used to reduced the position of the Perlin point
        float reduction = 0.5f;

        //Used in the smoothing process
        Vector2Int currentPos, lastPos;
        //The corresponding points of the smoothing. One list for x and one for y
        List<int> noiseX = new List<int>();
        List<int> noiseY = new List<int>();

        //Generate the noise
        for (int x = 0; x < map.GetUpperBound(0); x += interval)
        {
            newPoint = Mathf.FloorToInt((Mathf.PerlinNoise(x, (seed * reduction))) * map.GetUpperBound(1));
            noiseY.Add(newPoint);
            noiseX.Add(x);
        }

        points = noiseY.Count;

        //Start at 1 so we have a previous position already
        for (int i = 1; i < points; i++)
        {
            //Get the current position
            currentPos = new Vector2Int(noiseX[i], noiseY[i]);
            //Also get the last position
            lastPos = new Vector2Int(noiseX[i - 1], noiseY[i - 1]);

            //Find the difference between the two
            Vector2 diff = currentPos - lastPos;

            //Set up what the height change value will be
            float heightChange = diff.y / interval;
            //Determine the current height
            float currHeight = lastPos.y;

            //Work our way through from the last x to the current x
            for (int x = lastPos.x; x < currentPos.x; x++)
            {
                for (int y = Mathf.FloorToInt(currHeight); y > 0; y--)
                {
                    map[x, y] = 1;
                }
                currHeight += heightChange;
            }
        }
        return map;
    }

}
