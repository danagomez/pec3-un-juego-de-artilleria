using UnityEngine;

public class MusicClass : MonoBehaviour
{
    /**
     * Aquesta classe existeix per controlar la musica de fons (per a que no deixi de sonar quan es reinicia el nivell)
     * Tamb� controla que no hi hagi GameObjects duplicats de si mateix.
     */
    private GameObject[] other;
    private bool notFirst = false;

    private void Awake()
    {
        other = GameObject.FindGameObjectsWithTag("Music");

        foreach (GameObject oneOther in other)
        {
            if (oneOther.scene.buildIndex == -1)
                notFirst = true;
        }

        if (notFirst)
            Destroy(gameObject);

        DontDestroyOnLoad(transform.gameObject);
    }
}
