using System;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    // Classe per desar i consultar l'estat actual del jugador
    public class PlayerState
    {
        public bool grounded = false;
        public bool shooting = false;
        public bool hasTurn = true;
        public int health = GameManager.MaxHealth;
    }

    public PlayerState state;

    private Rigidbody2D rb;

    private float moveForce;
    private float downForce;
    private static float velocitat;
    private float currentPower;
    protected float currentAngle;
    private bool reverse;

    private Transform turret;
    private GameObject bulletPrefab;

    private Transform isGroundedChecker;
    private float checkGroundRadius;
    [SerializeField] private LayerMask groundLayer;

    private GameObject pathPointPrefab;
    private GameObject[] pathPoints;
    private int totalPathPoints;
    private float pathTime;

    public Animator playerAnimator;

    public AudioSource[] sound;
    /* 0 -> Engine Idle
     * 1 -> Engine Moving
     * 2 -> Turret Aim
     * 3 -> Destroyed
     * 4 -> Hit
     */

    void Start()
    {
        state = new PlayerState();
        rb = GetComponent<Rigidbody2D>();

        moveForce = 15f;
        downForce = 90f;
        velocitat = 5f;
        currentPower = 0;
        reverse = false;
        currentAngle = 0;

        turret = transform.Find("Pivot");
        bulletPrefab = Resources.Load<GameObject>("Prefabs/Bullet");

        isGroundedChecker = transform.Find("GroundChecker").GetComponent<Transform>();
        checkGroundRadius = 0.1f;

        pathTime = 0.05f;
        totalPathPoints = 50;
        pathPointPrefab = Resources.Load<GameObject>("Prefabs/PathPoint");
        pathPoints = new GameObject[totalPathPoints];

        // Instancia els punts de prediccio de traject�ria i els amaga
        for (int i = 0; i < totalPathPoints; i++)
        {
            pathPoints[i] = Instantiate(pathPointPrefab, transform.position, Quaternion.identity);
            pathPoints[i].SetActive(false);
        }

        sound = GetComponents<AudioSource>();
        playerAnimator = GetComponent<Animator>();
    }

    private void Update()
    {
        Disparar();

        if (Input.GetButton("Horizontal") && !sound[1].isPlaying)
            sound[1].Play();
        else if (Input.GetButtonUp("Horizontal") && sound[1].isPlaying)
            sound[1].Stop();
    }

    private void FixedUpdate()
    {
        CheckIfGrounded();

        Moviment();

        rb.AddForce(-transform.up * downForce);
    }

    void Moviment()
    {
        float x = Input.GetAxisRaw("Horizontal");

        if (x * rb.velocity.x < velocitat)
        {
            rb.AddForce(Vector2.right * x * moveForce);
            rb.velocity = new Vector2(x * velocitat, rb.velocity.y);
        }
    }

    private void Disparar()
    {
        turret.rotation = Quaternion.Euler(0, 0, transform.eulerAngles.z + currentAngle);

        // Quan mantenim la tecla d'espai, comen�em a apuntar
        if (Input.GetKey(KeyCode.Space))
        {
            state.shooting = true;

            // Mentres es mante pressionat cap bot� vertical, calcula l'angle
            if (Input.GetButton("Vertical"))
            {
                CalculateAngle();
                if (!sound[2].isPlaying)
                    sound[2].Play();
            }
            else if (Input.GetButtonUp("Vertical"))
                sound[2].Stop();

            CalculatePower();

            // Calculem la posici� de cada punt per la predicci� de traject�ria i els fem visibles
            for (int i = 0; i < totalPathPoints; i++)
            {
                pathPoints[i].SetActive(true);
                pathPoints[i].transform.position = PointPosition(i * pathTime);
            }
        }
        // Quan deixem anar la tecla d'espai, disparem
        if (Input.GetKeyUp(KeyCode.Space))
        {
            FireProjectile();

            // Un cop hem disparat, amaguem els punts de predicci�
            for (int i = 0; i < totalPathPoints; i++)
            {
                pathPoints[i].SetActive(false);
            }

            StartCoroutine(ChangeTurn());
        }
    }

    private void CalculateAngle()
    {
        /**
         * Aquest m�tode s'encarrega de calcular l'angle de la torreta del jugador.
         */
        if (Input.GetKey(KeyCode.UpArrow) || Input.GetKey(KeyCode.W))
            currentAngle += 0.5f;
        else if (Input.GetKey(KeyCode.DownArrow) || Input.GetKey(KeyCode.S))
            currentAngle -= 0.5f;

        currentAngle = Mathf.Clamp(currentAngle, 0, 75);

    }

    private void CalculatePower()
    {
        /**
         * Aquest m�tode s'encarrega de calcular la for�a a utilitzar.
         * Augmentar� i es reduir� autom�ticament entre els valors 10 i 70.
         */
        if (!reverse && currentPower <= 100)
        {
            if (currentPower == 70)
                reverse = true;
            currentPower += 0.6f;
        }
        else
        {
            reverse = true;
            if (currentPower == 10)
                reverse = false;
            currentPower -= 0.6f;
        }

        currentPower = Mathf.Clamp(currentPower, 10, 70);
    }

    private void FireProjectile()
    {
        /**
         * Aquest m�tode s'encarrega de disparar el projectil.
         * Crea el projectil en la posici� de la torreta i l'impulsa amb la for�a actual.
         */
        GameObject bullet = Instantiate(bulletPrefab, turret.position, turret.rotation);
        bullet.GetComponent<Bullet>().Initialize(currentPower, true);
    }

    Vector2 PointPosition(float time)
    {
        /**
         * Aquest m�tode s'encarrega de posicionar els punts per la predicci� de la traject�ria del projectil.
         * Primer, obt� l'angle actual en forma de Vector2.
         * Despr�s, calcula la posici� de cada punt fent servir la forumla seg�ent: 
         * P = posicio inicial + velocitat (angle * for�a) * temps + at^2 / 2 (fent 0.5 * el valor de gravity * quadrat de temps)
         */
        Vector2 angle = new Vector2((float)Math.Cos(currentAngle * Math.PI / 180), (float)Math.Sin(currentAngle * Math.PI / 180));
        Vector2 currentPointPosition = (Vector2)turret.position + (angle * (currentPower / 4) * time) + 0.5f * Physics2D.gravity * (time * time);

        return currentPointPosition;
    }

    private IEnumerator ChangeTurn()
    {
        /**
         * Aquest metode s'encarrega de canviar de torn de manera fluida perqu� no sigui massa r�pid o abrupte.
         */
        yield return new WaitForSeconds(1.5f);

        currentAngle = 0;
        currentPower = 0;
        state.shooting = false;
        state.hasTurn = false;
    }

    void CheckIfGrounded()
    {
        Collider2D colliders = Physics2D.OverlapCircle(isGroundedChecker.position, checkGroundRadius, groundLayer);

        if (colliders != null)
            state.grounded = true;
        else
            state.grounded = false;
    }

}
