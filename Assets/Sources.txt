Sprites:
Platformer Art Deluxe: https://kenney.nl/assets/platformer-art-deluxe
Tanks: https://kenney.nl/assets/tanks
Backgrounds: https://opengameart.org/content/2d-desert-platformer-pack

Fonts:
Teko (font): https://fonts.google.com/specimen/Teko

Sounds:
Engine sounds: https://opengameart.org/content/engine-loop-heavy-vehicletank
Action Shooter sounds: https://opengameart.org/content/action-shooter-soundset-wwvi
High Quality Explosions: https://opengameart.org/content/2-high-quality-explosions
13 Ambient Machine Sounds: https://opengameart.org/content/13-ambient-machine-sounds
GUI Sound Effects: https://opengameart.org/content/gui-sound-effects
BGM: 【東方Electro Swing】 VOLUPTÉ 「マホトア豆腐店」

Scripts:
Script de generació de terreny: https://blog.unity.com/technology/procedural-patterns-you-can-use-with-tilemaps-part-i
BackgroundParallax.cs: Obtingut del template de la PEC 3