
# PEC 3 - Un Juego de Artilleria  
  
Aquest joc es un joc d'artilleria basat en el Scorched Earth i Worms. Ha sigut creat per a un projecte del curs de Programació de Videojocs 2D de la UOC.
  
Clica [aquí](https://youtu.be/ibpafyQKvOQ) per un vídeo de demostració del joc.  
  
Clica [aquí](https://danagomezbalada.itch.io/pec-3-scorched-earth-worms) per jugar a la versió de WebGL online.  

## Funcionalitats del joc  
  
Un cop executem el joc, entrarem directament a l'escena principal del joc, però veurem un menú d'inici per sobre, i el joc no començarà fins que escollim alguna opció. En aquest menu veurem dos botons:  
- **Començar Joc**: Començarà el joc i avançarà el temps.  
- **Sortir Del Joc**: Es tancarà el joc.  
  
També veurem a la part superior el **títol** del joc. Podem navegar per aquest menú amb el teclat i el rató.  
  
![Foto del menú inicial](https://gitlab.com/danagomez/pec3-un-juego-de-artilleria/-/raw/master/Images/MainMenu.png)
  
Un cop comença el joc principal, escoltarem la **música de fons** i veurem al nostre tanc sobre un terreny generat automàticament a l'inici del joc. Al fons veurem unes muntanyes i el sol.
A la **part superior** podem veure tres marcadors:  
- **Barres de vida**: Són les barres de vida restant del jugador (a l'esquerra) i l'enemic (a la dreta). Tenen un màxim de **5 punts de vida** i s'actualitzen automàticament a mesura que perdem punts de vida.
- **Temps total**: És el contador del temps restant de la partida actual. Disminueix automàticament i mostra els **segons restants**.  
  
![Foto inicial del joc](https://gitlab.com/danagomez/pec3-un-juego-de-artilleria/-/raw/master/Images/Joc.png) 
  
Podem moure el personatge de la següent manera:  
- **Moviment horitzontal**: prement les tecles "*A*" o "*D*", o bé les fletxes *d'esquerra* i *dreta*.  
- **Apuntar**: **mantenint** la tecla *d'espai*. També podem mouren's **horitzontalment** mentre apuntem. Mentres estem apuntant, podem moure la torreta amb les tecles **verticals** (*W* o *S*, o bé fletxes *dalt* i *baix*).
- **Disparar**: **deixant anar** la tecla *d'espai* fará que disparem un projectil.
  
La **cámara** segueix al jugador que tingui el torn actual i quan aquest apunta, la cámara s'allunyarà per donar més visibilitat. Quan el jugador dispara, la cámara es manté allunyada un segon i després segueix al següent jugador.
  
Un cop el personatge amb el turn dispara, el **turn canvia** a l'altre. 

![Foto del projectil](https://gitlab.com/danagomez/pec3-un-juego-de-artilleria/-/raw/master/Images/Missil.PNG)
  
L'**enemic** es mou **automàticament**, fent servir intel·ligència artificial. Primer, l'enemic es mourà horitzontalment durant **2 segons** cap a una direcció **aleatòria** (esquerra o dreta), després començarà a **apuntar** amb la torreta. Un cop acabi d'apuntar, **dispararà** un projectil que té un 50% de probabilitat de **caure sobre el jugador**, i un 50% de probabilitat de **caure en alguna posició aleatòria**.
  
![Foto de l'explosió](https://gitlab.com/danagomez/pec3-un-juego-de-artilleria/-/raw/master/Images/Damage.PNG)
  
Quan es dispara, **apareixerà un projectil** que serà impulsat cap a la direcció corresponent (*dreta* pel jugador, *esquerra* per l'enemic). Darrere d'aquest projectil veurem una **línia que el segueix**.

Un cop el projectil **colisiona** o **se li acaba el temps de vida** (3 segons), aquest **desapareix** i veurem una **explosió amb partícules**. A més, si el projectil ha explotat sobre un dels jugadors o a prop, aquest **perd un punt de vida**.
  
![Foto del final del joc](https://gitlab.com/danagomez/pec3-un-juego-de-artilleria/-/raw/master/Images/EndMenu.PNG)

El joc acaba quan un dels jugadors es quedi **sense punts de vida o quan el temps s'acaba**. Quan acabi el joc, el temps es detendrà i veurem un menú final amb **dos botons** i les **estadístiques finals**.

Els botons ens permetràn **jugar de nou**, o **sortir del joc**.
A les **estadistiques** finals veurem:

 - El **temps emprat** en aquesta partida
 - Els **cops que ha rebut el jugador**
 - Els **cops que ha rebut l'enemic**
 - El **número de partides** jugades

## Organització del joc
Aquest joc s'ha creat amb l'editor de Unity. S'han utilitzat algunes **eines especials** per a certs aspectes:

 - Per la creació del nivell s'ha fet servir *Tilemaps* i "*Tilemap Extras*" per la **generació del nivell**.
 - S'ha fet servir un sistema de *partícules* per a les **explosions**, i un *Trail Renderer* per la **línia de seguiment** de projectil.
 - S'han utilitzat *tags* i *layers* per fer **l'organització** i **utilització** dels elements del joc més senzilla i independent.
 - S'han utilitzat **elements visuals** que mostren informació del joc, com ara la barra de vida de cada jugador i el temps restant.
	 - Per la **barra de vida**, s'ha fet servir un element "*Slider*".

## Explicació del codi 
  
### BackgroundParallax.cs 
Aquest script s'encarrega de controlar l'efecte **parallax del fons**.  
  
### GenerateTerrain.cs
Aquest script s'encarrega de **generar el terreny** del nivell **aleatòriament**, segons uns parametres i utilitzant el *RuleTile* de "*Tilemap Extras*".

### CameraScript.cs
Aquest script, assignat a la càmera del joc, s'encarrega de **seguir al jugador o enemic** depenent de qui té el turn. Quan un dels dos comença a apuntar, **s'allunya** i es **mou cap a la dreta o esquerra** (depenent de qui té el torn) per donar més visibilitat. Tots els moviments de la cámara són fluits gràcies als métodes ***Slerp*** de *Vector3* i ***MoveTowards*** de *Mathf*.

### MusicClass.cs
Aquesta classe existeix per **controlar la música de fons** (per a que **no deixi de sonar** quan es reinicia el nivell). També controla que no hi hagi *GameObjects* duplicats de si mateix.
  
### PlayerController.cs  
Aquest script s'encarrega principalment del **moviment i la lògica** del jugador. També controla l'**estat actual del jugador** en tot moment. Les funcions principals d'aquest script són:  
 - Comprovar si el jugador està **tocant el terra**. Fent servir el *Transform* d'un objecte buit dins del jugador i el *layer* del terra.  
 - Comprovar si estem prement alguna tecla.  
	- **Moviment horitzontal**: Podem moure el jugador polsant les tecles de moviment horitzontal (A o D, o bé fletxa esquerra o dreta).  
	- **Apuntar**: si el jugador **manté polsada** la tecla d'espai, començarà a apuntar. Mentre apuntem també comprova si estem prement cap botó **vertical** i, si és el cas, calcula l'angle de la torreta del jugador.
	- **Disparar**: quan el jugador **deixi anar** la tecla d'espai, disparará un projectil. El joc instància aquest objecte en l'angle donat i l'impulsa amb la força donada.
 - **Lògica mentre apuntem**: mentres estem apuntant, el joc fa varis càlculs que ens ajuden a disparar el projectil:
	- **Calcula l'àngle** en el qual sortirà el projectil, fent servir l'angle de la torreta.
	- **Calcula la força** a utilitzar quan disparem. Augmentarà i es reduirà automàticament entre els valors 10 i 70.
	- **Posiciona varis punts de predicció de la trajectòria del projectil**, ajudant al jugador a apuntar amb més exactitut.
		- Primer, obté l'angle actual en forma de Vector2.
		- Després, calcula la posició de cada punt fent servir la forumla següent: 
		- P = posicio inicial + velocitat *(angle * força)* * temps + at^2 / 2 *(fent 0.5 * el valor de gravity * quadrat de temps)*
 - Controlar l'**estat actual** del jugador, amb la classe ***PlayerState***, en la qual tenim les variables ***grounded***, ***shooting***, ***hasTurn*** i ***health***. Aquesta classe és pot accedir des d'altres scripts.
 - Controlar les **animacions** i **sons** del personatge del jugador. Depenent de l'acció actual, l'animació canviarà (quiet, movent-se, apuntant, fent-se dany, o morint).  
  
### EnemyScript.cs  
Aquest script s'encarrega de controlar el **moviment i la lògica** dels enemics. També controla l'**estat actual** de l'enemic en tot moment. Les funcions principals d'aquest script són:  
 - **Moure l'enemic automàticament**:
	- Inicialment, l'enemic es **mourà aleatòriament** cap a l'esquerra o dreta durant 2 segons. 
	- Un cop deixi de moure's, començarà a **apuntar**:
		- Si l'**angle actual** és més alt que l'**angle predefinit** (35), apuntarà més baix. Si és més baix, apuntarà més alt.
	- Un cop acabi d'apuntar, **dispararà** un projectil:
		- Primer, **calcula quanta força utilitzar** (segons la posició del jugador i l'angle predefinit).
		- Després, **crea el projectil** en la posició de la torreta i **l'impulsa** amb la força calculada anteriorment.
		- L'enemic te un **50% de probabilitat d'apuntar correctament** al jugador. Si no, apuntarà a una direcció aleatòria.
 - Controlar l'**estat actual** de l'enemic fent servir la classe ***EnemyState***, en la qual tenim les variables: ***moving***, ***aiming***, ***shooting***, ***hasTurn*** i ***health***. Aquesta classe és pot accedir des d'altres scripts.
 - Controlar les **animacions** i **sons** de l'enemic. Depenent de l'acció actual, l'animació canviarà (quiet, movent-se, apuntant, fent-se dany, o morint).
  
### Bullet.cs  
Aquest script s'encarrega de controlar la **lògica del projectil**, així com la **creació de l'explosió**. Un cop s'instància un projectil, aquest defineix un **temps de vida restant** (3 segons) fent servir el métode *Invoke()*. Després de ser instanciat, **s'inicialitza** el projectil:
 - Primer, **decideix a quina direcció apuntar** (depenent de si dispara el jugador o l'enemic).
 - Després, **s'impulsa amb la força rebuda** i fent servir *Impulse* de *ForceMode2D*.

Un cop s'inicialitza, **actualitza la seva rotació** per donar l'efecte de que va caient. Un cop el projectil **colisiona**: explota, comproba si la col·lisió ha sigut **amb el jugador o l'enemic** (dins del radi) i crida el métode de ***GameManager*** per fer dany.

Quan el projectil **explota**, s'encarrega de **crear l'explosió** (fent servir el **sistema de partícules**). Després, destrueix el projectil (a si mateix) i l'explosio després d'un segon.
  
### GameManager.cs  
Aquest script s'encarrega de controlar alguns dels **aspectes principals** del joc, així com els **elements de UI**, l'**estat del joc** i els **comptadors**. Les funcions d'aquest script són:

 - **Iniciar** el joc principal, activant el temps i utilitzant un ***InvokeRepeating()*** per reduir el temps restant. També s'encarrega de **reiniciar** el joc o **sortir** del joc.
 - Controlar **qui té el torn actual**, indicant a la cámara a qui seguir i **desactivar el script** del oponent que no tingui el torn.
 - Controlar **si el jugador ha reinicat el joc**. Si és així, el menú d'inici no es mostrarà i s'entrarà directament al joc.
 - Controlar el "**fi del joc**" (en el qual el script del jugador és *deshabilitarà* i els comptadors és *detindran*), amb totes les seves possibilitats:  
	- **Si el jugador no té punts de vida**, s'espera a que acabi l'animació de morir i mostra el menú final amb el text de "**Has perdut!**" i les estadístiques finals.
	- **Si l'enemic no té punts de vida**, s'espera a que acabi l'animació de morir i mostra el menú final amb el text de "**Has guanyat!**" i les estadístiques finals.
	- **Si s'ha acabat el temps**, mostra el menú final amb el text de "**S'ha acabat el temps!**" i les estadístiques finals.
 - Controlar **si un dels jugadors rep dany**. S'encarrega de restar punts de vida al jugador o l'enemic. També **actualitza les barres de vida** del UI, i utilitza l'animació i so adients segons la situació (distingeix entre només rebre dany o morir).
 - Controlar els **comptadors** de *vida dels jugadors* i el *temps restant*. 
	- **Barres de vida**: Utilitzant l'element Slider, actualitza les barres de vida dels jugadors segons els seus punts de vida restants.  
	- **Temps**: Mostra els **segons restants** de la partida.
  
### GameStats.cs  
Aquesta classe s'encarrega de desar les **estadistiques** finals del joc *(temps emprat, cops que ha rebut el jugador, cops que ha rebut l'enemic, partides jugades totals*). També controla si el jugador ha **reiniciat** la partida.
  
## Assets utilitzats  

### Sprites:
#### Platformer Art Deluxe: https://kenney.nl/assets/platformer-art-deluxe
#### Tanks: https://kenney.nl/assets/tanks
#### Backgrounds: https://opengameart.org/content/2d-desert-platformer-pack

### Fonts:
#### Teko (font): https://fonts.google.com/specimen/Teko

### Sounds:
#### Engine sounds: https://opengameart.org/content/engine-loop-heavy-vehicletank
#### Action Shooter sounds: https://opengameart.org/content/action-shooter-soundset-wwvi
#### High Quality Explosions: https://opengameart.org/content/2-high-quality-explosions
#### 13 Ambient Machine Sounds: https://opengameart.org/content/13-ambient-machine-sounds
#### GUI Sound Effects: https://opengameart.org/content/gui-sound-effects
#### BGM: 【東方Electro Swing】 VOLUPTÉ 「マホトア豆腐店」

### Scripts:
#### Script de generació de terreny: https://blog.unity.com/technology/procedural-patterns-you-can-use-with-tilemaps-part-i
#### BackgroundParallax.cs: Obtingut del template de la PEC 3